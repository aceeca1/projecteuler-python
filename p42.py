import lib.Polygonal

with open('data/input42.txt') as file:
    a = [v[1:-1] for v in file.readline()[:-1].split(',')]
print(sum(1
    for ai in a
    if lib.Polygonal.isa(3, sum(ord(i) - ord('A') + 1 for i in ai))))
