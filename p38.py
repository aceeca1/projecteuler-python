def isPan(a):
    s = 0
    for i in a: s |= 1 << (ord(i) - ord('0'))
    return s == 0x3fe
ans = ""
for i in range(1, 10000):
    s, k = str(i), i + i
    while len(s) < 9:
        s += str(k)
        k += i
    if len(s) == 9 and isPan(s):
        ans = max(ans, s)
print(ans)
