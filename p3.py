def factors(n):
    m = 2
    while m * m <= n:
        while n % m == 0:
            n /= m
            yield m
        m += 1
    if n > 1: yield n

ans = 0
for ans in factors(600851475143): pass
print(ans)
