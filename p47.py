import lib.Prime
n, h = 1, 0
while h < 4:
    if len(set(lib.Prime.factorize(n))) == 4: h += 1
    else: h = 0
    n += 1
print(n - 4)
