def isPalindrome(n):
    s = str(n)
    return s[::-1] == s

ans = 0
for i1 in range(100, 1000):
    for i2 in range(100, 1000):
        n = i1 * i2
        if isPalindrome(n): ans = max(ans, n)
print(ans)
