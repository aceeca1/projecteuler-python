import lib.Polygonal

i = 144
while True:
    a = lib.Polygonal.num(6, i)
    if lib.Polygonal.isa(5, a) and lib.Polygonal.isa(3, a):
        ans = a
        break
    i += 1
print(ans)
