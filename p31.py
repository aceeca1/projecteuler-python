n = 200
a = [0] * (n + 1)
a[0] = 1
for i in [1, 2, 5, 10, 20, 50, 100, 200]:
    for j in range(i, n + 1):
        a[j] += a[j - i]
print(a[n])