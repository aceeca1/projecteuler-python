ans = 0
a = 1
while a <= 1000:
    s1 = 1000 - a
    s2 = a * a
    if s2 % s1 == 0:
        b2 = s1 - s2 / s1
        if b2 < 0 or b2 % 2 == 0:
            b = b2 / 2
            c = s1 - b
            ans = a * b * c
            break
    a += 1
print(ans)
