ans = [0]
ms = [2, 3, 5, 7, 11, 13, 17]
def put(a, m):
    if m == -1:
        s = 0
        for i in a: s = s * 10 + i
        ans[0] += s
    elif m >= 7 or (a[m + 1] * 100 + a[m + 2] * 10 + a[m + 3]) % ms[m] == 0:
        am = a[m]
        for i in range(0, m + 1):
            a[m] = a[i]
            a[i] = am
            put(a, m - 1)
            a[i] = a[m]
put(list(range(0, 10)), 9)
print(ans[0])
