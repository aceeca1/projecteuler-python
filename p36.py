def isPalindrome(s):
    return s == s[::-1]
print(sum(i
    for i in range(1, 1000000)
    if isPalindrome(str(i)) and isPalindrome(bin(i)[2:])))
