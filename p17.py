pronC = {
    1 : 'one'      , 2 : 'two'       , 3 : 'three'    ,
    4 : 'four'     , 5 : 'five'      , 6 : 'six'      ,
    7 : 'seven'    , 8 : 'eight'     , 9 : 'nine'     ,
    10: 'ten'      , 11: 'eleven'    , 12: 'twelve'   ,
    13: 'thirteen' , 14: 'fourteen'  , 15: 'fifteen'  ,
    16: 'sixteen'  , 17: 'seventeen' , 18: 'eighteen' ,
    19: 'nineteen' , 20: 'twenty'    , 30: 'thirty'   ,
    40: 'forty'    , 50: 'fifty'     , 60: 'sixty'    ,
    70: 'seventy'  , 80: 'eighty'    , 90: 'ninety'   ,
    1000: 'oneThousand'
}

def pron(n):
    if n in pronC: return len(pronC[n])
    if n < 100: return len(pronC[n // 10 * 10]) + len(pronC[n % 10])
    if n % 100 == 0: return len(pronC[n / 100]) + len('hundred')
    return pron(n - n % 100) + len('and') + pron(n % 100)

ans = 0
for i in range(1, 1001): ans += pron(i)
print(ans)
