import lib.Prime
import heapq

class TwoSSeq:
    def __init__(self, x1):
        self.x1 = x1
        self.x2 = -1
        self.moveNext()
    def moveNext(self):
        self.x2 += 1
        self.v = self.x1 + (self.x2 * self.x2 << 1)
    def __lt__(self, other):
        return self.v < other.v

class PrimeTwoSSeq:
    def __init__(self):
        self.a = 2
        self.qu = [TwoSSeq(2)]
    def v(self):
        return self.qu[0].v
    def moveNext(self):
        quMin = heapq.heappop(self.qu)
        if quMin.x1 == self.a:
            self.a = lib.Prime.next(self.a)
            heapq.heappush(self.qu, TwoSSeq(self.a))
        quMin.moveNext()
        heapq.heappush(self.qu, quMin)

a = PrimeTwoSSeq()
i = 3
while True:
    while a.v() < i: a.moveNext()
    if a.v() != i:
        ans = i
        break
    i += 2
print(ans)
