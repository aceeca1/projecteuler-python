m = 10000000000

def pow(a1, a2):
    ans = 1
    for _ in range(a2): ans = ans * a1 % m
    return ans
    
ans = 0
for i in range(1, 1001): ans += pow(i, i)
print(ans % m)

    