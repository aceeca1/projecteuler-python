import lib.Divisor

ans = 1
for i in range(1, 21):
    ans = ans * (i / lib.Divisor.gcd(ans, i))
print(ans)
