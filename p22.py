with open('data/input22.txt') as file:
    a = [v[1:-1] for v in file.readline()[:-1].split(',')]
a.sort()
ans = 0
for i in range(0, len(a)):
    ans += (i + 1) * sum((ord(j) - ord('A') + 1) for j in a[i])
print(ans)
