def isPan(a):
    s = 0
    for i in a:
        for j in str(i):
            s |= 1 << (ord(j) - ord('0'))
    return s == 0x3fe

a = set()

def f(n1, n2):
    for i1 in range(n1, n1 * 10):
        for i2 in range(n2, n2 * 10):
            i3 = i1 * i2
            if i3 >= 10000: break
            if isPan([i1, i2, i3]): a.add(i3)

f(10, 100)
f(1, 1000)
print(sum(a))
