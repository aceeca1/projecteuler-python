n = 15
with open('data/input18.txt') as file:
    a = [list(map(int, file.readline().split())) for _ in range(0, n)]
ans = a[n - 1]
for m in range(n - 2, -1, -1):
    ans = [max(ans[i], ans[i + 1]) + a[m][i] for i in range(0, m + 1)]
print(ans[0])
