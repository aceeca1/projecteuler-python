n = 20
with open('data/input11.txt') as file:
    a = [list(map(int, file.readline().split())) for i in range(0, 20)]
dx = [0, 1, 1,  1]
dy = [1, 0, 1, -1]

def prod(x, y, d):
    m = 0
    ans = 1
    while True:
        if m == 4: return ans
        elif not(0 <= x < n and 0 <= y < n): return 0
        else: 
            ans *= a[x][y]
            m += 1
            x += dx[d]
            y += dy[d]

ans = 0
for i in range(0, n):
    for j in range(0, n):
        for k in range(0, 4):
            ans = max(ans, prod(i, j, k))
print(ans)
