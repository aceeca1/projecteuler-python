a = [i * i * i * i * i for i in range(0, 10)]
print(sum(n 
    for n in range(2, 6 * a[9] + 1) 
    if n == sum(a[ord(i) - ord('0')] for i in str(n))))
