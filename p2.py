import lib.Fibonacci

ans = 0
for i in lib.Fibonacci.startsWith(1, 1):
    if i > 4000000: break
    if i & 1 == 0: ans += i
print(ans)
