class ProdZ:
    def __init__(self):
        self.pd = 1
        self.z = 0
    def add(self, c):
        if c == '0': self.z += 1
        else: self.pd *= ord(c) - ord('0')
    def del_(self, c):
        if c == '0': self.z -= 1
        else: self.pd /= ord(c) - ord('0')

sb = ''
with open("data/input8.txt") as file:
    for i in range(0, 20): sb += file.readline()[:-1]
pz = ProdZ()
ans = 0
for i in range(0, 13): pz.add(sb[i])
for i in range(13, len(sb) + 1): 
    if pz.z == 0: ans = max(ans, pz.pd)
    if i != len(sb):
        pz.add(sb[i])
        pz.del_(sb[i - 13])
print(ans)
