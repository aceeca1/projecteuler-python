def choose(n, k):
    ret = 1
    for i in range(1, k + 1):
        ret = ret * (n - i + 1) / i
    return ret

print(choose(40, 20))
