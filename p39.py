ans, pAns = 0, -1
for p in range(1, 1001):
    s = 0
    for a in range(1, p):
        a2 = a * a
        bpc = p - a
        if a2 % bpc == 0:
            cmb = a2 // bpc
            bb = bpc - cmb
            if bb & 1 == 0:
                b = bb >> 1
                if a < b: s += 1
    if s > ans: ans, pAns = s, p
print(pAns)
