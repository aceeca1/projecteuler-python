def next(n): 
    if n & 1 == 0: return n >> 1
    else: return n * 3 + 1

a = [0] * 1000001
a[1] = 1

def chainLen(n):
    if n > 1000000: return chainLen(next(n)) + 1
    elif a[n]: return a[n]
    else: 
        a[n] = chainLen(next(n)) + 1
        return a[n]

ans = 0
for i in range(1, 1000001):
    if chainLen(i) > a[ans]: ans = i
print(ans)
