import lib.Divisor

a, b = 1, 1
for n1 in range(1, 10):
    for n0 in range(0, 10):
        n = n1 * 10 + n0
        for m1 in range(1, 10):
            m0 = n1
            m = m1 * 10 + m0
            if m != n and m * n0 == n * m1:
                a *= m
                b *= n
print(b / lib.Divisor.gcd(a, b))
