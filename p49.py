import lib.Hash
import lib.Prime

u = {}
a = lib.Prime.sieve(10000)
i = 1000
while a[i] < i: i += 1
while i < 10000:
    h = sum(lib.Hash.h64(ord(j)) for j in str(i))
    if h not in u: u[h] = []
    u[h].append(i)
    i = a[i]
for ui in u.values():
    for i1 in range(len(ui)):
        for i2 in range(i1 + 1, len(ui)):
            for i3 in range(i2 + 1, len(ui)):
                if ui[i2] - ui[i1] == ui[i3] - ui[i2]:
                    if ui[i1] != 1487:
                        print("%d%d%d" % (ui[i1], ui[i2], ui[i3]))
