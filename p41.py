import lib.Prime

def isPan(n):
    a = str(n)
    s = 0
    for i in a: s |= 1 << (ord(i) - ord('0'))
    return s == (((1 << len(a)) - 1) << 1)

a = lib.Prime.sieve(10000000)
ans = -1
i = 2
while i < 10000000:
    if isPan(i): ans = i
    i = a[i]
print(ans)
