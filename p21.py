import lib.Divisor

def isAmicable(n):
    m = lib.Divisor.sumOf(n)
    return n != m and n == lib.Divisor.sumOf(m)
    
print(sum(i for i in range(1, 10001) if isAmicable(i)))
