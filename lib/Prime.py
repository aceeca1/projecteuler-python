import math

def sieve(n):
    ret = [0] * (n + 1)
    u = 0
    for i in range(2, n + 1):
        v = ret[i]
        if v == 0: v = ret[u] = u = i
        w = 2
        while i * w <= n:
            ret[i * w] = w
            if w >= v: break
            w = ret[w]
    ret[u] = n + 1
    return ret

class Sieve:
    a = sieve(1048576)
    @staticmethod
    def doubleA():
        Sieve.a = sieve((len(Sieve.a) - 1) << 1)
    @staticmethod
    def ensure(n):
        while n >= len(Sieve.a): Sieve.doubleA()

def isa(n):
    if n < 2: return False
    Sieve.ensure(n)
    return Sieve.a[n] > n

def isaL(n):
    if n < 2: return False
    m = int(math.sqrt(n))
    Sieve.ensure(m)
    i = 2
    while i <= m:
        if n % i == 0: return False
        i = Sieve.a[i]
    return True

def next(n):
    Sieve.ensure(n * 6 / 5)
    return Sieve.a[n]

def factorize(n):
    Sieve.ensure(n)
    ret = []
    if n == 1: return ret
    while True:
        p = Sieve.a[n]
        if p > n:
            ret.append(n)
            return ret
        else:
            ret.append(p)
            n //= p
