def find(n, f):
    idx1, n1, idx2, n2 = 0, n, 1, f(n)
    while True:
        if n1 == n2: return idx2 - idx1
        if idx2 > idx1 + idx1: idx1, n1 = idx2, n2
        idx2, n2 = idx2 + 1, f(n2)
