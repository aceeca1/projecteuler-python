import math

def num(k, n):
    return n * (n - 1) // 2 * (k - 2) + n

def isa(k, n):
    if k == 3:
        return n == num(k, int(math.sqrt(n + n)))
    elif k == 4:
        return n == num(k, int(math.sqrt(n)))
    else:
        return n == num(k, int(math.sqrt(float(n + n) / (k - 2))) + 1)
