def startsWith(m0, m1):
    while True:
        yield m0
        m0, m1 = m1, m0 + m1
