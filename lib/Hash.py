def h128to64(u1, u2):
    kMul = 0x9ddfea08eb382d69
    kMax = 0xffffffffffffffff
    a1 = ((u1 ^ u2) * kMul) & kMax
    a2 = a1 ^ (a1 >> 47)
    b1 = ((a2 ^ u1) * kMul) & kMax
    b2 = b1 ^ (b1 >> 47)
    return b2 * kMul & kMax

def h64(u):
    return h128to64(u, 0)
