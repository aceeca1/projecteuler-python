def gcd(a1, a2):
    while True:
        if a2 == 0: return a1
        a1, a2 = a2, a1 % a2

def ofNum(n):
    i = 1
    while i * i < n:
        if n % i == 0:
            yield n / i
            yield i
        i += 1
    if i * i == n: yield i

def sumOf(n):
    return sum(ofNum(n)) - n
