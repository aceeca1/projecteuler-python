import lib.Cycle

ans, iAns = 0, -1
for i in range(1, 1001):
    c = lib.Cycle.find(1, lambda u: u * 10 % i)
    if c > ans: ans, iAns = c, i
print(iAns)
