import lib.Prime

n = 1000000
a = lib.Prime.sieve(n)
ans, iAns = 0, -1
i = 2
while i < n and i * ans < n:
    s = i
    j = a[i]
    while j < n and s < n:
        s += j
        if s < n and a[s] > s:
            len = j - i + 1
            if len > ans: ans, iAns = len, s
        j = a[j]
    i = a[i]
print(iAns)
