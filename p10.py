import lib.Prime

n = 2000000
a = lib.Prime.sieve(n)
ans = 0
i = 2
while i < n:
    ans += i
    i = a[i]
print(ans)
