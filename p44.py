import lib.Divisor
import lib.Polygonal

# (* {a, b, a + b, a - b} are pentagonal. Let c = a - b, and they becomes
# {b, c, c + b, c + 2b}. Let c + b = 3q(q - 1) / 2, b = 3s(s - 1) / 2.
# We have (3q + 3s + 1)(q - s) = 2c.

ans = 0
try:
    i = 2
    while True:
        c = lib.Polygonal.num(5, i)
        c2 = c + c
        for k1 in lib.Divisor.ofNum(c2):
            k2 = k1 + 1
            if k2 % 3 == 0:
                k3 = k2 // 3
                k4 = c2 // k1
                if k3 > k4:
                    k5 = k3 + k4
                    if k5 % 2 == 0:
                        q = k5 / 2
                        s = k3 - q
                        a = lib.Polygonal.num(5, q)
                        b = lib.Polygonal.num(5, s)
                        if lib.Polygonal.isa(5, a + b):
                            ans = c
                            raise StopIteration
        i += 1
except StopIteration:
    print(ans)
