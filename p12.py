import lib.Divisor

n = 1
m = 2
while sum(1 for _ in lib.Divisor.ofNum(n)) <= 500: 
    n += m
    m += 1
print(n)
