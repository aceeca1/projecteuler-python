import lib.Prime
n = 1000000
a = lib.Prime.sieve(n)
ps = set()
i = 2
while i < n:
    ps.add(str(i))
    i = a[i]

def rotate(s, n):
    m = n % len(s)
    return s[m:] + s[:m]

for i in [4, 2, 1]:
    ps = set(k for k in ps if rotate(k, i) in ps)
print(len(ps))
