fac = [0] * 10
fac[0] = 1
for i in range(1, 10): fac[i] = fac[i - 1] * i
ans = 0
print(sum(i
    for i in range(10, fac[9] * 7 + 1)
    if i == sum(fac[ord(j) - ord('0')] for j in str(i))))
