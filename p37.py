import lib.Prime
ans = [0]

def put(m):
    for i in [1, 3, 7, 9]:
        mi = m * 10 + i
        if lib.Prime.isaL(mi): put(mi)
    i = 10
    while i < m:
        if not lib.Prime.isaL(m % i): return
        i *= 10
    ans[0] += m

for i in [2, 3, 5, 7]: put(i)
print(ans[0] - 2 - 3 - 5 - 7)
