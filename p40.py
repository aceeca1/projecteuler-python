a = set([1, 10, 100, 1000, 10000, 100000, 1000000])
ans, c, n = 1, 1, 1
while c <= 1000000:
    for i in str(n):
        if c in a: ans *= ord(i) - ord('0')
        c += 1
    n += 1
print(ans)
