import lib.Prime
ans, iAns = 0, -1
for a in range(-999, 1000):
    for b in range(-1000, 1001):
        n = 0
        while lib.Prime.isa((n + a) * n + b): n += 1
        if n > ans: ans, iAns = n, a * b
print(iAns)
