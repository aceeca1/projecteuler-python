import lib.Divisor
n = 28123
ab = [i for i in range(1, n + 1) if lib.Divisor.sumOf(i) > i]
a = [False] * (n + 1)
for i in range(0, len(ab)):
    for j in range(i, len(ab)):
        s = ab[i] + ab[j]
        if s >= len(a): break
        a[s] = True
print(sum(i for i in range(0, len(a)) if not a[i]))
