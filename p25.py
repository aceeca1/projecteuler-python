import lib.Fibonacci

for i, v in enumerate(lib.Fibonacci.startsWith(1, 1)):
    if len(str(v)) >= 1000: break
print(i + 1)
