import lib.Prime
import math

n = 10000
m = int(n * math.log(n) * 1.5)
a = lib.Prime.sieve(m)
ans = 2
for i in range(2, 10002): ans = a[ans]
print(ans)
